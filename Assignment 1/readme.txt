Topic: Finding head person

Keyboard Control:

'w' - Moving head to the far plane
's' - Moving head to the near plane
'a' - Moving head to the left
'd' - Moving head to the right

'z' - Rotating head clockwisely
'x' - Rotating head anti-clockwisely

'q' - exit

'spacebar' - The body will move up and then the head will be back to the body

P.S. After this animation, users are only allowed to click 'q' to exit
