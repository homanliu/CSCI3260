// Name:  Liu Ho Man
// SID:   1155077343
// CSCI3260 Assignment 1

#ifdef __APPLE__
  #include <GL/glew.h>
  #include <GLUT/glut.h>
  #include <stdlib.h>
  #include <time.h>
  #include <math.h>
  #include "Dependencies/glm/glm.hpp"
  #include "Dependencies/glm/gtc/matrix_transform.hpp"
  #include "Dependencies/glm/gtx/rotate_vector.hpp"
  #include "Dependencies/glm/gtx/vector_angle.hpp"
  #define GL_INIT_CONTEXT() do {  glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH | GLUT_MULTISAMPLE); } while(0)
#endif

#ifdef _WIN32__
	#include "Dependencies/glew/glew.h"
	#include "Dependencies/freeglut/freeglut.h"
	#include "Dependencies/glm/glm.hpp"
	#include "Dependencies/glm/gtc/matrix_transform.hpp"
#endif

#include <iostream>
#include <fstream>
#include <time.h>
#include <cmath>
#include <ctime>
using namespace std;
//using glm::vec3;
//using glm::mat4;

GLuint programID;
GLuint cubeVAO, planeVAO, pyramidVAO, rectangleVAO;
GLuint cubeVBO, planeVBO, pyramidVerticesVBO, pyramidIndicesVBO, rectangleVerticesVBO, rectangleIndicesVBO;

float x_delta = 0.1f, z_delta = 0.1f;
int x_press_num = 0, z_press_num = 0;
float rotation_theta = 0.0f;
bool animated = false, startAnimation = false;
int startTime = 0, switchCase = 0;
int timeRotate = 0;

std::clock_t start;

glm::mat4 autoBodyRotationMatrix = glm::mat4(1.0f);
glm::mat4 autoLeftArmRotationMatrix = glm::mat4(1.0f);
glm::mat4 autoRightArmRotationMatrix = glm::mat4(1.0f);
glm::mat4 autoHeadRotationMatrix = glm::mat4(1.0f);

void sendDataToOpenGL(){
  const GLfloat floorPlane[] = {
    //     Vertex                    Colour
    -1.0f, +0.0f, -1.0f,      +1.0f, +1.0f, +1.0f,
    -1.0f, +0.0f, +1.0f,      +1.0f, +1.0f, +1.0f,
    +1.0f, +0.0f, -1.0f,      +1.0f, +1.0f, +1.0f,
    -1.0f, +0.0f, +1.0f,      +1.0f, +1.0f, +1.0f,
    +1.0f, +0.0f, +1.0f,      +1.0f, +1.0f, +1.0f,
    +1.0f, +0.0f, -1.0f,      +1.0f, +1.0f, +1.0f,};

	const GLfloat cube[] = {
    //     Vertex                    Colour
    // cube bottom plane
    -0.5f, -0.5f, -0.5f,      +1.0f, +0.5f, +1.0f,
		-0.5f, -0.5f, +0.5f,      +0.0f, +1.0f, +1.0f,
		+0.5f, -0.5f, -0.5f,      +0.0f, +0.0f, +1.0f,
    -0.5f, -0.5f, +0.5f,      +0.0f, +1.0f, +1.0f,
		+0.5f, -0.5f, +0.5f,      +1.0f, +1.0f, +0.0f,
		+0.5f, -0.5f, -0.5f,      +0.0f, +0.0f, +1.0f,
    // cube bottom plane

    // cube upper plane
    -0.5f, +0.5f, -0.5f,      +1.0f, +1.0f, +0.0f,
		-0.5f, +0.5f, +0.5f,      +1.0f, +1.0f, +0.0f,
		+0.5f, +0.5f, -0.5f,      +1.0f, +0.0f, +1.0f,
    -0.5f, +0.5f, +0.5f,      +1.0f, +1.0f, +0.0f,
		+0.5f, +0.5f, +0.5f,      +1.0f, +0.0f, +1.0f,
		+0.5f, +0.5f, -0.5f,      +1.0f, +0.0f, +1.0f,
    // cube upper plane

    // cube front plane
    -0.5f, +0.5f, +0.5f,      +1.0f, +1.0f, +0.0f,
		-0.5f, -0.5f, +0.5f,      +0.0f, +1.0f, +1.0f,
		+0.5f, +0.5f, +0.5f,      +1.0f, +0.0f, +1.0f,
    -0.5f, -0.5f, +0.5f,      +0.0f, +1.0f, +1.0f,
		+0.5f, -0.5f, +0.5f,      +1.0f, +1.0f, +0.0f,
		+0.5f, +0.5f, +0.5f,      +1.0f, +0.0f, +1.0f,
    // cube front plane

    // cube back plane
    -0.5f, +0.5f, -0.5f,      +1.0f, +1.0f, +0.0f,
		-0.5f, -0.5f, -0.5f,      +1.0f, +0.5f, +1.0f,
		+0.5f, +0.5f, -0.5f,      +1.0f, +0.0f, +1.0f,
    -0.5f, -0.5f, -0.5f,      +1.0f, +0.5f, +1.0f,
		+0.5f, -0.5f, -0.5f,      +1.0f, +0.0f, +0.0f,
		+0.5f, +0.5f, -0.5f,      +1.0f, +0.0f, +1.0f,
    // cube back plane

    // cube right plane
    +0.5f, +0.5f, +0.5f,      +1.0f, +0.0f, +1.0f,
		+0.5f, -0.5f, +0.5f,      +1.0f, +1.0f, +0.0f,
		+0.5f, +0.5f, -0.5f,      +1.0f, +0.0f, +1.0f,
    +0.5f, -0.5f, +0.5f,      +1.0f, +1.0f, +0.0f,
		+0.5f, -0.5f, -0.5f,      +1.0f, +0.0f, +0.0f,
		+0.5f, +0.5f, -0.5f,      +1.0f, +0.0f, +1.0f,
    // cube right plane

    // cube left plane
    -0.5f, +0.5f, +0.5f,      +1.0f, +1.0f, +0.0f,
		-0.5f, -0.5f, +0.5f,      +0.0f, +1.0f, +1.0f,
		-0.5f, +0.5f, -0.5f,      +1.0f, +1.0f, +0.0f,
    -0.5f, -0.5f, +0.5f,      +0.0f, +1.0f, +1.0f,
		-0.5f, -0.5f, -0.5f,      +1.0f, +0.5f, +1.0f,
		-0.5f, +0.5f, -0.5f,      +1.0f, +1.0f, +0.0f,

    +0.25f, -0.25f, +0.52f,   +0.0f, +0.0f, +0.0f,
    +0.25f, -0.1f, +0.52f,    +0.0f, +0.0f, +0.0f,
    +0.1f, -0.25f, +0.52f,    +0.0f, +0.0f, +0.0f,
    +0.1f, -0.25f, +0.52f,    +0.0f, +0.0f, +0.0f,
    +0.25f, -0.1f, +0.52f,    +0.0f, +0.0f, +0.0f,
    +0.1f, -0.1f, +0.52f,     +0.0f, +0.0f, +0.0f,

    -0.25f, -0.25f, +0.52f,   +0.0f, +0.0f, +0.0f,
    -0.25f, -0.1f, +0.52f,    +0.0f, +0.0f, +0.0f,
    -0.1f, -0.25f, +0.52f,    +0.0f, +0.0f, +0.0f,
    -0.1f, -0.25f, +0.52f,    +0.0f, +0.0f, +0.0f,
    -0.25f, -0.1f, +0.52f,    +0.0f, +0.0f, +0.0f,
    -0.1f, -0.1f, +0.52f,     +0.0f, +0.0f, +0.0f,

    +0.1f, +0.3f, +0.52f,     +0.0f, +0.0f, +0.0f,
    -0.1f, +0.3f, +0.52f,     +0.0f, +0.0f, +0.0f,
    +0.0f, +0.0f, +0.52f,     +0.0f, +0.0f, +0.0f,};

  const GLfloat pyramid[] = {
    //     Vertex                    Colour
    +0.0f, +1.0f, +0.0f,      +1.0f, +0.8f, +0.2f,
    -0.3f, +0.0f, +0.3f,      +0.7f, +1.0f, +0.3f,
    +0.0f, +0.0f, -0.3f,      +0.2f, +1.0f, +0.1f,
    +0.3f, +0.0f, +0.3f,      +0.1f, +1.0f, +0.8f,};

  GLushort pyramidIndices[] = {
    0, 1, 3,      0, 1, 2,
    0, 2, 3,      1, 2, 3,};

  const GLfloat rectangle[] = {
    //     Vertex                    Colour
    +0.5f, -0.2f, -0.2f,      +1.0f, +0.2f, +0.4f,
    -0.5f, -0.2f, -0.2f,      +1.0f, +0.3f, +0.3f,
    -0.5f, -0.2f, +0.2f,      +0.1f, +1.0f, +0.7f,
    +0.5f, -0.2f, +0.2f,      +0.2f, +1.0f, +0.2f,
    +0.5f, +0.2f, -0.2f,      +0.4f, +1.0f, +0.9f,
    -0.5f, +0.2f, -0.2f,      +0.2f, +0.6f, +1.0f,
    -0.5f, +0.2f, +0.2f,      +0.7f, +0.4f, +1.0f,
    +0.5f, +0.2f, +0.2f,      +0.1f, +0.7f, +1.0f,
  };

  GLushort rectangleIndices[] = {
    0, 1, 2,      0, 2, 3,
    4, 5, 6,      4, 6, 7,
    4, 5, 1,      4, 1, 0,
    7, 1, 2,      7, 2, 3,
    4, 7, 3,      4, 3, 0,
    1, 5, 6,      1, 6, 2,
  };

  // For cube
	glGenVertexArrays(1, &cubeVAO);
	glBindVertexArray(cubeVAO);
	glGenBuffers(1, &cubeVBO);
	glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (char*)(3 * sizeof(float)));

  // For plane
  glGenVertexArrays(1, &planeVAO);
	glBindVertexArray(planeVAO);
  glGenBuffers(1, &planeVBO);
	glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(floorPlane), floorPlane, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (char*)(3 * sizeof(float)));

  // For pyramid
  glGenVertexArrays(1, &pyramidVAO);
	glBindVertexArray(pyramidVAO);
  // Bind Vertices Data to Buffer
  glGenBuffers(1, &pyramidVerticesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, pyramidVerticesVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid), pyramid, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (char*)(3 * sizeof(float)));
  // Bind Indices Data to Buffer
  glGenBuffers(1, &pyramidIndicesVBO);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pyramidIndicesVBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(pyramidIndices) * sizeof(GLushort), pyramidIndices, GL_STATIC_DRAW);

  // For rectangle
  glGenVertexArrays(1, &rectangleVAO);
	glBindVertexArray(rectangleVAO);
  // Bind Vertices Data to Buffer
  glGenBuffers(1, &rectangleVerticesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, rectangleVerticesVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangle), rectangle, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (char*)(3 * sizeof(float)));
  // Bind Indices Data to Buffer
  glGenBuffers(1, &rectangleIndicesVBO);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pyramidIndicesVBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(rectangleIndices) * sizeof(GLushort), rectangleIndices, GL_STATIC_DRAW);

}

bool checkStatus(
	GLuint objectID,
	PFNGLGETSHADERIVPROC objectPropertyGetterFunc,
	PFNGLGETSHADERINFOLOGPROC getInfoLogFunc,
	GLenum statusType){
	GLint status;
	objectPropertyGetterFunc(objectID, statusType, &status);
	if (status != GL_TRUE)
	{
		GLint infoLogLength;
		objectPropertyGetterFunc(objectID, GL_INFO_LOG_LENGTH, &infoLogLength);
		GLchar* buffer = new GLchar[infoLogLength];

		GLsizei bufferSize;
		getInfoLogFunc(objectID, infoLogLength, &bufferSize, buffer);
		cout << buffer << endl;

		delete[] buffer;
		return false;
	}
	return true;
}

bool checkShaderStatus(GLuint shaderID){
	return checkStatus(shaderID, glGetShaderiv, glGetShaderInfoLog, GL_COMPILE_STATUS);
}

bool checkProgramStatus(GLuint programID){
	return checkStatus(programID, glGetProgramiv, glGetProgramInfoLog, GL_LINK_STATUS);
}

string readShaderCode(const char* fileName){
	ifstream meInput(fileName);
	if (!meInput.good())
	{
		cout << "File failed to load..." << fileName;
		exit(1);
	}
	return std::string(
		std::istreambuf_iterator<char>(meInput),
		std::istreambuf_iterator<char>()
	);
}

void installShaders(){
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* adapter[1];
	//adapter[0] = vertexShaderCode;
	string temp = readShaderCode("VertexShaderCode.glsl");
	adapter[0] = temp.c_str();
	glShaderSource(vertexShaderID, 1, adapter, 0);
	//adapter[0] = fragmentShaderCode;
	temp = readShaderCode("FragmentShaderCode.glsl");
	adapter[0] = temp.c_str();
	glShaderSource(fragmentShaderID, 1, adapter, 0);

	glCompileShader(vertexShaderID);
	glCompileShader(fragmentShaderID);

	if (!checkShaderStatus(vertexShaderID) || !checkShaderStatus(fragmentShaderID))
		return;

	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	glLinkProgram(programID);

	if (!checkProgramStatus(programID))
		return;
	glUseProgram(programID);

}

void initializedGL(void){
	sendDataToOpenGL();
	installShaders();
}

void resetKey(void){
  startTime = floor((std::clock() - start)/1000);
  if(startTime >= 10){
    if(x_press_num > 0)
      x_press_num -= 1;
    else if(x_press_num < 0)
      x_press_num += 1;
    if(z_press_num > 0)
      z_press_num -= 1;
    else if(z_press_num < 0)
      z_press_num += 1;
    if(round(rotation_theta * 10)/10.0f > 0.0f)
      rotation_theta -= 0.1f;
    else if(round(rotation_theta * 10)/10.0f < 0.0f)
      rotation_theta += 0.1f;
    if(x_press_num == 0 && z_press_num == 0 && round(rotation_theta * 10)/10.0f == 0.0f)
      switchCase = 2;
    startTime = 0;
  }
}

void bodyUp(void){
  autoBodyRotationMatrix = glm::inverse(glm::scale(glm::mat4(), glm::vec3(1.7f, 1.0f, 1.0f))) *
                           glm::inverse(glm::translate(glm::mat4(), glm::vec3(0.0f, -0.2f, -1.0f))) *
                           glm::inverse(glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(0, 1, 0)));
  autoBodyRotationMatrix = glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(0, 0, 1)) * autoBodyRotationMatrix;
  autoBodyRotationMatrix = glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(0, 1, 0)) *
                           glm::translate(glm::mat4(), glm::vec3(0.0f, -0.5f, -1.0f)) *
                           glm::scale(glm::mat4(), glm::vec3(1.7f, 1.0f, 1.0f)) *
                           autoBodyRotationMatrix;

  autoLeftArmRotationMatrix = glm::inverse(glm::scale(glm::mat4(), glm::vec3(1.3f, 1.0f, 1.0f))) *
                              glm::inverse(glm::translate(glm::mat4(), glm::vec3(0.4f, -0.2f, -0.1f))) *
                              glm::inverse(glm::rotate(glm::mat4(), -7.0f, glm::vec3(0, 1, 0)));
  autoLeftArmRotationMatrix = glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(0, 0, 1)) * autoLeftArmRotationMatrix;
  autoLeftArmRotationMatrix = glm::translate(glm::mat4(), glm::vec3(0.45f, -0.4f, 0.3f)) *
                              glm::rotate(glm::mat4(), glm::radians(-50.0f), glm::vec3(0, 0, 1)) *
                              glm::scale(glm::mat4(), glm::vec3(1.3f, 1.0f, 1.0f)) *
                              autoLeftArmRotationMatrix;

  autoRightArmRotationMatrix = glm::inverse(glm::scale(glm::mat4(), glm::vec3(1.3f, 1.0f, 1.0f))) *
                               glm::inverse(glm::translate(glm::mat4(), glm::vec3(-0.4f, -0.2f, -0.1f))) *
                               glm::inverse(glm::rotate(glm::mat4(), 7.0f, glm::vec3(0, 1, 0)));
  autoRightArmRotationMatrix = glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(0, 0, 1)) * autoRightArmRotationMatrix;
  autoRightArmRotationMatrix = glm::translate(glm::mat4(), glm::vec3(0.35f, -0.4f, 0.3f)) *
                               glm::rotate(glm::mat4(), glm::radians(50.0f), glm::vec3(0, 0, 1)) *
                               glm::scale(glm::mat4(), glm::vec3(1.3f, 1.0f, 1.0f)) *
                               autoRightArmRotationMatrix;
  switchCase = 3;
  start = std::clock();
}

void headPlug(void){
  float x, y;
  startTime = floor((std::clock() - start)/1000);
  if(startTime <= 75){
    x = startTime / 50.0f;
    y = 5.0f * x * (x - 1.8f);
  }
  else if(startTime > 75 && startTime <= 87){
    x = 1.5f;
    y = -2.25f + 0.1f * (startTime - 75);
  }
  else if(startTime > 87){
    x = 1.5f;
    y = -2.25f + 1.2f;
    switchCase = 4;
    startTime = 0;
  }
  autoHeadRotationMatrix = glm::inverse(glm::scale(glm::mat4(), glm::vec3(0.8f, 0.8f, 0.8f))) *
                           glm::inverse(glm::translate(glm::mat4(), glm::vec3(-1.5f, -1.0f, 1.0f))) *
                           glm::inverse(glm::rotate(glm::mat4(), 0.0f, glm::vec3(0, 1, 0)));
  autoHeadRotationMatrix = glm::rotate(glm::mat4(), 0.0f, glm::vec3(0, 1, 0)) *
                           glm::translate(glm::mat4(), glm::vec3(-1.5f + x, -1.0f + y, 1.0f)) *
                           glm::scale(glm::mat4(), glm::vec3(0.8f, 0.8f, 0.8f)) *
                           autoHeadRotationMatrix;

}

void matrixMVPTRS(string objects){
  float planeConstant = -1.0f;
  glm::mat4 modelScalingMatrix = glm::mat4(1.0f);
  glm::mat4 modelTransformMatrix = glm::mat4(1.0f);
  glm::mat4 modelRotationMatrix = glm::mat4(1.0f);

  if(objects == "plane"){
    modelScalingMatrix = glm::scale(glm::mat4(), glm::vec3(3.0f, 1.0f, 3.0f));
  }
  else if(objects == "pyramid"){
    modelScalingMatrix = glm::scale(glm::mat4(), glm::vec3(-3.3f, -0.6f, -1.5f));
  }
  else if(objects == "cube"){
    modelScalingMatrix = glm::scale(glm::mat4(), glm::vec3(0.8f, 0.8f, 0.8f));
  }
  else if(objects == "rectangle"){
    modelScalingMatrix = glm::scale(glm::mat4(), glm::vec3(1.7f, 1.0f, 1.0f));
  }
  else if(objects == "leftFeet" || objects == "rightFeet"){
    modelScalingMatrix = glm::scale(glm::mat4(), glm::vec3(1.3f, 1.0f, 1.0f));
  }

  if(objects == "plane"){
    modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, planeConstant));;
  }
  else if(objects == "cube"){
    modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(-1.5f + x_press_num * x_delta, -1.0f, planeConstant + 2.0f + z_press_num * z_delta));;
  }
  else if(objects == "rectangle"){
    modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, -0.2f, planeConstant));;
  }
  else if(objects == "pyramid"){
    modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(1.8f, -0.2f, planeConstant + 2.7f));;
  }
  else if(objects == "leftFeet"){
    modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(0.3f, -0.2f, planeConstant + 1.2f));;
  }
  else if(objects == "rightFeet"){
    modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(-0.3f, -0.2f, planeConstant + 1.2f));;
  }
  else if(objects == "leftHand"){
    modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(0.4f, -0.2f, planeConstant));;
  }
  else if(objects == "rightHand"){
    modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(-0.4f, -0.2f, planeConstant));;
  }

  if(objects == "rectangle"){
    modelRotationMatrix = autoBodyRotationMatrix * glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(0, 1, 0));
  }
  else if(objects == "leftFeet"){
    modelRotationMatrix = glm::rotate(glm::mat4(), glm::radians(120.0f), glm::vec3(0, 1, 0));
  }
  else if(objects == "rightFeet"){
    modelRotationMatrix = glm::rotate(glm::mat4(), glm::radians(60.0f), glm::vec3(0, 1, 0));
  }
  else if(objects == "leftHand"){
    modelRotationMatrix = autoLeftArmRotationMatrix * glm::rotate(glm::mat4(), -7.0f, glm::vec3(0, 1, 0));
  }
  else if(objects == "rightHand"){
    modelRotationMatrix = autoRightArmRotationMatrix * glm::rotate(glm::mat4(), 7.0f, glm::vec3(0, 1, 0));
  }
  else if(objects == "cube"){
    modelRotationMatrix = autoHeadRotationMatrix * glm::rotate(glm::mat4(), rotation_theta, glm::vec3(0, 1, 0));
  }

  GLint modelScalingMatrixUniformLocation = glGetUniformLocation(programID, "modelScalingMatrix");
  GLint modelTransformMatrixUniformLocation = glGetUniformLocation(programID, "modelTransformMatrix");
  GLint modelRotateMatrixUniformLocation = glGetUniformLocation(programID, "modelRotationMatrix");

  glUniformMatrix4fv(modelScalingMatrixUniformLocation, 1, GL_FALSE, &modelScalingMatrix[0][0]);
  glUniformMatrix4fv(modelTransformMatrixUniformLocation, 1, GL_FALSE, &modelTransformMatrix[0][0]);
	glUniformMatrix4fv(modelRotateMatrixUniformLocation, 1, GL_FALSE, &modelRotationMatrix[0][0]);

  // Projection matrix : 45° Field of View
  glm::mat4 Projection = glm::perspective(30.0f, 1.0f, 2.0f, 20.0f);
  glm::mat4 View = glm::lookAt(
      glm::vec3(0,-2.0,0), // Camera is at (x,y,z), in World Space
      glm::vec3(0,0,-11), // and looks at point
      glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
      );
  glm::mat4 Model = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.5f, -5.0f));;
  glm::mat4 mvp = Projection * View * Model;
  GLuint MatrixID = glGetUniformLocation(programID, "MVP");
  glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);
}

void paintGL(void){  //always run

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //specify the background color
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Enable depth test
  glClearDepth(1.0f);
  glDepthFunc(GL_LESS);
  glEnable(GL_DEPTH_TEST);
  // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  matrixMVPTRS("cube");
  glBindVertexArray(cubeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 51); //render primitives from array data

  matrixMVPTRS("plane");
  glBindVertexArray(planeVAO);
  glDrawArrays(GL_TRIANGLES, 0, 6); //render primitives from array data

  matrixMVPTRS("pyramid");
  glBindVertexArray(pyramidVAO);
  glDrawElements(GL_TRIANGLES, 3 * 4 * sizeof(GLushort), GL_UNSIGNED_SHORT, nullptr);

  matrixMVPTRS("rectangle");
  glBindVertexArray(rectangleVAO);
  glDrawElements(GL_TRIANGLES, 3 * 12 * sizeof(GLushort), GL_UNSIGNED_SHORT, nullptr);

  matrixMVPTRS("leftFeet");
  glBindVertexArray(rectangleVAO);
  glDrawElements(GL_TRIANGLES, 3 * 12 * sizeof(GLushort), GL_UNSIGNED_SHORT, nullptr);

  matrixMVPTRS("rightFeet");
  glBindVertexArray(rectangleVAO);
  glDrawElements(GL_TRIANGLES, 3 * 12 * sizeof(GLushort), GL_UNSIGNED_SHORT, nullptr);

  matrixMVPTRS("leftHand");
  glBindVertexArray(rectangleVAO);
  glDrawElements(GL_TRIANGLES, 3 * 12 * sizeof(GLushort), GL_UNSIGNED_SHORT, nullptr);

  matrixMVPTRS("rightHand");
  glBindVertexArray(rectangleVAO);
  glDrawElements(GL_TRIANGLES, 3 * 12 * sizeof(GLushort), GL_UNSIGNED_SHORT, nullptr);

  glutSwapBuffers();
	glFlush(); //force execution of GL commands
	glutPostRedisplay();

  if(startAnimation){
    if(switchCase == 1)
      resetKey();
    else if(switchCase == 2)
      bodyUp();
    else if(switchCase == 3)
      headPlug();
    else if(switchCase == 4)
      startAnimation = false;
  }

}

void keyboard(unsigned char key, int x, int y){
	if (key == 'a' && !animated)
	  x_press_num += 1;
	if (key == 'd' && !animated)
	  x_press_num -= 1;
  if (key == 'w' && !animated)
	  z_press_num -= 1;
	if (key == 's' && !animated)
	  z_press_num += 1;
  if (key == 'q')
    exit(0);
  if (key == 'z' && !animated)
    rotation_theta += 0.1f;
	if (key == 'x' && !animated)
	  rotation_theta -= 0.1f;
  if (key == 32 && !animated){
    animated = true;
    startAnimation = true;
    start = std::clock();
    switchCase = 1;
  }
}

int main(int argc, char* argv[]){
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);
	glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH | GLUT_MULTISAMPLE);
	glutInitWindowSize(512, 512);
	//glutInitWindowPosition()
	glutCreateWindow(argv[0]); // window title
	glewInit();

	initializedGL();

	glutDisplayFunc(paintGL);
	glutKeyboardFunc(keyboard);

	glutMainLoop(); //call display callback over and over
	return 0;
}
