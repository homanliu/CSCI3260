#version 410

layout(location=0) in vec3 position;
layout(location=1) in vec3 vertexColor;

uniform mat4 modelScalingMatrix;
uniform mat4 modelTransformMatrix;
uniform mat4 modelRotationMatrix;
uniform mat4 MVP;

out vec3 theColor;

void main()
{
	vec4 v = vec4(position, 1.0);
	// MVPTRS = Model * View * Projection * Transform * Rotate * Scale
	vec4 new_position = MVP * modelTransformMatrix * modelRotationMatrix * modelScalingMatrix * v;
	gl_Position = new_position;
	theColor = vertexColor;
}
