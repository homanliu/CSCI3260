#include "Dependencies/glm/glm.hpp"
#include "Dependencies/glm/gtc/matrix_transform.hpp"
#include "Dependencies/glm/gtx/rotate_vector.hpp"
#include "Dependencies/glm/gtx/vector_angle.hpp"
#include <GLUT/glut.h>

#include <iostream>
#include <cmath>

using namespace std;

extern float specularCoefficient, diffuseCoefficient, planeRender;

extern float carX, carZ, carRotate;
extern int rotatePress, xOrigin, yOrigin;
extern float rotation_theta, rotation_alpha;
extern float airplaneTheta;
extern bool startMouse, mousePosinit, startAnimation;

void keyboardClick(unsigned char key, int x, int y){
  if (key == '1')
    exit(0);
  else if (key == 'j')
    rotation_theta += 0.1f;
	else if (key == 'k')
	  rotation_theta -= 0.1f;
	else if (key == ' '){
		startMouse = !startMouse;
		mousePosinit = false;
	}
	else if (key == 'z'){
		specularCoefficient += 0.05f;
	}
	else if (key == 'x'){
		specularCoefficient -= 0.05f;
		specularCoefficient = glm::clamp(specularCoefficient, 0.0f, 100.0f);
	}
	else if (key == 'q'){
		diffuseCoefficient += 1.0f;
	}
	else if (key == 'w'){
		diffuseCoefficient -= 1.0f;
		diffuseCoefficient = glm::clamp(diffuseCoefficient, 1.0f, diffuseCoefficient);
	}
	else if (key == 's'){
		startAnimation = !startAnimation;
	}
}

void arrowKey(int key, int x, int y){
	if (key == GLUT_KEY_UP){
		carX += 0.1f * cos(carRotate * rotatePress);
		carZ += 0.1f * sin(carRotate * rotatePress);
	}
	else if (key == GLUT_KEY_DOWN){
		carX -= 0.1f * cos(carRotate * rotatePress);
		carZ -= 0.1f * sin(carRotate * rotatePress);
	}
	else if (key == GLUT_KEY_LEFT){
		rotatePress -= 1;
	}
	else if (key == GLUT_KEY_RIGHT){
		rotatePress += 1;
	}
}

void mouseCoordinate(int x, int y){
	if(!mousePosinit && startMouse){
		xOrigin = x;
		yOrigin = y;
		mousePosinit = !mousePosinit;
	}
	else if(startMouse && mousePosinit){
		rotation_theta = 45.0f + 0.01f * (x-xOrigin);
		rotation_alpha = 45.0f + 0.01f * (y-yOrigin);
		rotation_alpha = glm::clamp(rotation_alpha, 44.03f, 45.6f);
	}

}
