#version 410

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 bitangent;

uniform mat4 modelScalingMatrix;
uniform mat4 modelTransformMatrix;
uniform mat4 modelRotationMatrix;
uniform mat4 PVM;

uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform vec3 lightPosition;
uniform float planeRender;
uniform mat3 VM;

out vec2 UV;
out vec3 worldPos;
out vec3 cameraEye;
out vec3 lightDirection;
out vec3 cameraNormal;

out vec3 lightTangent;
out vec3 eyeTangent;

void main()
{
  vec4 v = vec4(position, 1.0f);
  vec4 new_position = PVM * modelTransformMatrix * modelRotationMatrix * modelScalingMatrix * v;
	gl_Position = new_position;
  UV = vertexUV;

  worldPos = (modelMatrix * v).xyz;

  vec3 cameraPos = (viewMatrix * modelMatrix * v).xyz;
  cameraEye = vec3(0.0f, 0.0f, 0.0f) - cameraPos;

  vec3 cameraLight = (viewMatrix * vec4(lightPosition, 1.0f)).xyz;
  lightDirection = cameraLight + cameraEye;

  cameraNormal = (viewMatrix * modelMatrix * vec4(normal, 0.0f)).xyz;

  if(planeRender == 1.0f){
    vec3 cameraTangent = VM * normalize(tangent);
    vec3 cameraBitangent = VM * normalize(bitangent);
    vec3 cameraNormal = VM * normalize(normal);
    mat3 TBN = transpose(mat3(cameraTangent, cameraBitangent, cameraNormal));

    lightTangent = TBN * lightDirection;
    eyeTangent = TBN * cameraEye;
  }
}
