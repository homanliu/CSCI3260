/*********************************************************
FILE : main.cpp (csci3260 2017-2018 Assignment 2)
*********************************************************/
/*********************************************************
Student Information
Student ID:		1155077343
Student Name:	Liu Ho Man
*********************************************************/

#define _CRT_SECURE_NO_DEPRECATE
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "Dependencies\glew\glew.h"
#include "Dependencies\freeglut\freeglut.h"
#include "Dependencies\glm\glm.hpp"
#include "Dependencies\glm\gtc\matrix_transform.hpp"
#define GL_INIT_CONTEXT() do {  glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH | GLUT_MULTISAMPLE); } while(0)


#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
using namespace std;
using glm::vec3;
using glm::mat4;

GLint programID;
GLuint planeVAO, kirbyVAO, jeepVAO, airplaneVAO;
GLuint vertexVBO, uvVBO, normalVBO;
GLsizei drawSize[4];

GLuint TextureID, TexturePlane, TextureKirby, TextureJeep, TextureAirplane;
GLuint textureNormal, textureSpecular, normalID, specularID;

float specularCoefficient = 0.1f, diffuseCoefficient = 50.0f, planeRender = 0.0f;

float carX = 0.0f, carZ = 0.0f, carRotate = 1.0f;
int rotatePress = 0, xOrigin, yOrigin;
float rotation_theta = 45.0f, rotation_alpha = 45.0f;
float airplaneTheta = 0.0f;
bool startMouse = false, mousePosinit = false, startAnimation = true;

bool checkStatus(
	GLuint objectID,
	PFNGLGETSHADERIVPROC objectPropertyGetterFunc,
	PFNGLGETSHADERINFOLOGPROC getInfoLogFunc,
	GLenum statusType){
	GLint status;
	objectPropertyGetterFunc(objectID, statusType, &status);
	if (status != GL_TRUE)
	{
		GLint infoLogLength;
		objectPropertyGetterFunc(objectID, GL_INFO_LOG_LENGTH, &infoLogLength);
		GLchar* buffer = new GLchar[infoLogLength];

		GLsizei bufferSize;
		getInfoLogFunc(objectID, infoLogLength, &bufferSize, buffer);
		cout << buffer << endl;

		delete[] buffer;
		return false;
	}
	return true;
}

bool checkShaderStatus(GLuint shaderID){
	return checkStatus(shaderID, glGetShaderiv, glGetShaderInfoLog, GL_COMPILE_STATUS);
}

bool checkProgramStatus(GLuint programID){
	return checkStatus(programID, glGetProgramiv, glGetProgramInfoLog, GL_LINK_STATUS);
}

string readShaderCode(const char* fileName){
	ifstream meInput(fileName);
	if (!meInput.good())
	{
		cout << "File failed to load..." << fileName;
		exit(1);
	}
	return std::string(
		std::istreambuf_iterator<char>(meInput),
		std::istreambuf_iterator<char>()
	);
}

void installShaders(){
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* adapter[1];
	string temp = readShaderCode("VertexShaderCode.glsl");
	adapter[0] = temp.c_str();
	glShaderSource(vertexShaderID, 1, adapter, 0);
	temp = readShaderCode("FragmentShaderCode.glsl");
	adapter[0] = temp.c_str();
	glShaderSource(fragmentShaderID, 1, adapter, 0);

	glCompileShader(vertexShaderID);
	glCompileShader(fragmentShaderID);

	if (!checkShaderStatus(vertexShaderID) || !checkShaderStatus(fragmentShaderID))
		return;

	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	glLinkProgram(programID);

	if (!checkProgramStatus(programID))
		return;

	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

	glUseProgram(programID);
}

void keyboard(unsigned char key, int x, int y){
  if (key == '1')
    exit(0);
  else if (key == 'j')
    rotation_theta += 0.1f;
	else if (key == 'k')
	  rotation_theta -= 0.1f;
	else if (key == ' '){
		startMouse = !startMouse;
		mousePosinit = false;
	}
	else if (key == 'z'){
		specularCoefficient += 0.05f;
	}
	else if (key == 'x'){
		specularCoefficient -= 0.05f;
		specularCoefficient = glm::clamp(specularCoefficient, 0.0f, 100.0f);
		cout << "{" << specularCoefficient << "}\n";
	}
	else if (key == 'q'){
		diffuseCoefficient += 1.0f;
	}
	else if (key == 'w'){
		diffuseCoefficient -= 1.0f;
		diffuseCoefficient = glm::clamp(diffuseCoefficient, 1.0f, diffuseCoefficient);
	}
	else if (key == 's'){
		startAnimation = !startAnimation;
	}
}

void move(int key, int x, int y){
	if (key == GLUT_KEY_UP){
		carX += 0.1f * cos(carRotate * rotatePress);
		carZ += 0.1f * sin(carRotate * rotatePress);
	}
	else if (key == GLUT_KEY_DOWN){
		carX -= 0.1f * cos(carRotate * rotatePress);
		carZ -= 0.1f * sin(carRotate * rotatePress);
	}
	else if (key == GLUT_KEY_LEFT){
		rotatePress -= 1;
	}
	else if (key == GLUT_KEY_RIGHT){
		rotatePress += 1;
	}
}

void PassiveMouse(int x, int y){
	if(!mousePosinit && startMouse){
		xOrigin = x;
		yOrigin = y;
		mousePosinit = !mousePosinit;
	}
	else if(startMouse && mousePosinit){
		rotation_theta = 45.0f + 0.01f * (x-xOrigin);
		rotation_alpha = 45.0f + 0.01f * (y-yOrigin);
		rotation_alpha = glm::clamp(rotation_alpha, 44.03f, 45.6f);
	}

}

bool loadOBJ(
	const char * path,
	std::vector<glm::vec3> & out_vertices,
	std::vector<glm::vec2> & out_uvs,
	std::vector<glm::vec3> & out_normals
	){
	printf("Loading OBJ file %s...\n", path);

	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;


	FILE * file = fopen(path, "r");
	if (file == NULL) {
		printf("Impossible to open the file ! Are you in the right path ? See Tutorial 1 for details\n");
		getchar();
		return false;
	}

	while (1) {

		char lineHeader[128];
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

				   // else : parse lineHeader

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0) {
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uv.y = -uv.y;
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				printf("File can't be read by our simple parser :-( Try exporting with other options\n");
				return false;
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
		else {
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}

	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i<vertexIndices.size(); i++) {

		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = uvIndices[i];
		unsigned int normalIndex = normalIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		// Put the attributes in buffers
		out_vertices.push_back(vertex);
		out_uvs.push_back(uv);
		out_normals.push_back(normal);

	}

	return true;
}

GLuint loadBMP_custom(const char * imagepath) {

	printf("Reading image %s\n", imagepath);

	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	unsigned char * data;

	FILE * file = fopen(imagepath, "rb");
	if (!file) { printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath); getchar(); return 0; }

	if (fread(header, 1, 54, file) != 54) {
		printf("Not a correct BMP file\n");
		return 0;
	}
	if (header[0] != 'B' || header[1] != 'M') {
		printf("Not a correct BMP file\n");
		return 0;
	}
	if (*(int*)&(header[0x1E]) != 0) {
    printf("Not a correct BMP file\n");
    return 0;
  }
	if (*(int*)&(header[0x1C]) != 24) {
    printf("Not a correct BMP file\n");
    return 0;
  }

	dataPos    = *(int*)&(header[0x0A]);
	imageSize  = *(int*)&(header[0x22]);
	width      = *(int*)&(header[0x12]);
	height     = *(int*)&(header[0x16]);
	if (imageSize == 0)
    imageSize = width*height * 3;
	if (dataPos == 0)
    dataPos = 54;

	data = new unsigned char[imageSize];
	fread(data, 1, imageSize, file);
	fclose(file);

  GLuint textureID;
  glGenTextures(1, &textureID);

  // "Bind" the newly created texture : all future texture functions will modify this texture
  glBindTexture(GL_TEXTURE_2D, textureID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  // Give the image to OpenGL
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);
	delete[] data;
  glGenerateMipmap(GL_TEXTURE_2D);

	return textureID;
}

void computeTangent(
	// inputs
	std::vector<glm::vec3> & vertices,
	std::vector<glm::vec2> & uvs,
	std::vector<glm::vec3> & normals,
	// outputs
	std::vector<glm::vec3> & tangents,
	std::vector<glm::vec3> & bitangents){
	for (unsigned int i=0; i<vertices.size(); i+=3 ){
		// Shortcuts for vertices
		glm::vec3 & v0 = vertices[i+0];
		glm::vec3 & v1 = vertices[i+1];
		glm::vec3 & v2 = vertices[i+2];

		// Shortcuts for UVs
		glm::vec2 & uv0 = uvs[i+0];
		glm::vec2 & uv1 = uvs[i+1];
		glm::vec2 & uv2 = uvs[i+2];

		// Edges of the triangle : postion delta
		glm::vec3 deltaPos1 = v1-v0;
		glm::vec3 deltaPos2 = v2-v0;

		// UV delta
		glm::vec2 deltaUV1 = uv1-uv0;
		glm::vec2 deltaUV2 = uv2-uv0;

		float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
		glm::vec3 tangent = (deltaPos1 * deltaUV2.y   - deltaPos2 * deltaUV1.y)*r;
		glm::vec3 bitangent = (deltaPos2 * deltaUV1.x   - deltaPos1 * deltaUV2.x)*r;

		// Set the same tangent for all three vertices of the triangle.
		// They will be merged later, in vboindexer.cpp
		tangents.push_back(tangent);
		tangents.push_back(tangent);
		tangents.push_back(tangent);

		// Same thing for binormals
		bitangents.push_back(bitangent);
		bitangents.push_back(bitangent);
		bitangents.push_back(bitangent);
	}

	// See "Going Further"
	for (unsigned int i=0; i<vertices.size(); i+=1){
		glm::vec3 & n = normals[i];
		glm::vec3 & t = tangents[i];
		glm::vec3 & b = bitangents[i];

		// Gram-Schmidt orthogonalize
		t = glm::normalize(t - n * glm::dot(n, t));

		// Calculate handedness
		if (glm::dot(glm::cross(n, t), b) < 0.0f)
			t = t * -1.0f;
	}
}

void matrixPVMTRS(string object){
  glm::mat4 modelScalingMatrix = glm::mat4(1.0f);
  glm::mat4 modelTransformMatrix = glm::mat4(1.0f);
  glm::mat4 modelRotationMatrix = glm::mat4(1.0f);
	glm::mat4 carModelSelfRotation = glm::rotate(glm::mat4(), glm::radians(carRotate * rotatePress), glm::vec3(0, 1, 0));
	glm::mat4 airplaneSelfRotation = glm::rotate(glm::mat4(), -airplaneTheta, glm::vec3(0, 1, 0)) * glm::rotate(glm::mat4(), -airplaneTheta, glm::vec3(0, 0, 1));
	glm::mat3 VM3x3 = glm::mat3(1.0f);
	// std::cout << "{" << carRotate * rotatePress << "}\n";

	if(object == "plane"){
  	modelScalingMatrix = glm::scale(glm::mat4(), glm::vec3(1.0f, 1.5f, 1.5f));
		modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, 0.0f));;
		modelRotationMatrix = glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(0, 1, 0));
	}
	else if(object == "jeep"){
		modelScalingMatrix = carModelSelfRotation * glm::scale(glm::mat4(), glm::vec3(0.5f, 0.5f, 0.5f));
		modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(2.0f + carX, 0.3f, -1.0f + carZ));;
		modelRotationMatrix = glm::mat4(1.0f);
	}
	else if(object == "airplane"){
		modelScalingMatrix = airplaneSelfRotation * glm::scale(glm::mat4(), glm::vec3(0.15f, 0.15f, 0.15f));
		modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(5.0f * cos(airplaneTheta), 7.0f, 5.0f * sin(airplaneTheta)));;
		modelRotationMatrix = glm::mat4(1.0f);
	}


  GLint modelScalingMatrixUniformLocation = glGetUniformLocation(programID, "modelScalingMatrix");
  GLint modelTransformMatrixUniformLocation = glGetUniformLocation(programID, "modelTransformMatrix");
  GLint modelRotateMatrixUniformLocation = glGetUniformLocation(programID, "modelRotationMatrix");

  glUniformMatrix4fv(modelScalingMatrixUniformLocation, 1, GL_FALSE, &modelScalingMatrix[0][0]);
  glUniformMatrix4fv(modelTransformMatrixUniformLocation, 1, GL_FALSE, &modelTransformMatrix[0][0]);
	glUniformMatrix4fv(modelRotateMatrixUniformLocation, 1, GL_FALSE, &modelRotationMatrix[0][0]);

  // Projection matrix : 45° Field of View
  glm::mat4 Projection = glm::perspective(glm::radians(45.0f), 1.0f, 2.0f, 40.0f);
  glm::mat4 View = glm::lookAt(
      glm::vec3(20.0f * cos(rotation_theta), 15.0f * sin(rotation_alpha), 20.0f * sin(rotation_theta)), // Camera is at (x,y,z), in World Space
			// glm::vec3(0.0f, 0.0f, 0.0f),
			glm::vec3(0.0f, 2.0f, 0.0f), // and looks at point
      glm::vec3(0.0f, 1.0f, 0.0f)  // Head is up (set to 0,-1,0 to look upside-down)
      );
  glm::mat4 Model = glm::mat4(1.0f);
  glm::mat4 pvm = Projection * View * Model;

	glm::vec3 lightPosition = glm::vec3(0.0f, 8.0f, -2.0f);
	GLuint LightID = glGetUniformLocation(programID, "lightPosition");
	glUniform3f(LightID, lightPosition.x, lightPosition.y, lightPosition.z);

	glm::mat3 VM = glm::mat3(View * Model);
	GLuint VMID = glGetUniformLocation(programID, "VM");
	glUniformMatrix4fv(VMID, 1, GL_FALSE, &VM[0][0]);

  GLuint MatrixID = glGetUniformLocation(programID, "PVM");
	GLuint ViewID = glGetUniformLocation(programID, "viewMatrix");
	GLuint ModelID = glGetUniformLocation(programID, "modelMatrix");
  glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &pvm[0][0]);
	glUniformMatrix4fv(ViewID, 1, GL_FALSE, &View[0][0]);
	glUniformMatrix4fv(ModelID, 1, GL_FALSE, &Model[0][0]);

	GLuint diffuseID = glGetUniformLocation(programID, "diffuseCoefficient");
	glUniform1f(diffuseID, diffuseCoefficient);
	GLuint specularID = glGetUniformLocation(programID, "specularCoefficient");
	glUniform1f(specularID, specularCoefficient);

	GLuint renderID = glGetUniformLocation(programID, "planeRender");
	glUniform1f(renderID, planeRender);
}

void sendDataToOpenGL(){

  std::vector <glm::vec3> vertices;
  std::vector <glm::vec2> uvs;
  std::vector <glm::vec3> normals;
	std::vector <glm::vec3> tangents;
	std::vector <glm::vec3> bitangents;

	// Load Plane Object file
  bool res = loadOBJ("plane.obj", vertices, uvs, normals);
	computeTangent(vertices, uvs, normals, tangents, bitangents);
  glGenVertexArrays(1, &planeVAO);
  glBindVertexArray(planeVAO);
  // Bind Vertices Data to Buffer
  glGenBuffers(1, &vertexVBO);
  glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind UV Data to Buffer
  glGenBuffers(1, &uvVBO);
  glBindBuffer(GL_ARRAY_BUFFER, uvVBO);
  glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind Normal Data to Buffer
	glGenBuffers(1, &normalVBO);
  glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
  glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec2), &normals[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  drawSize[0] = vertices.size();
	GLuint tangentVBO, bitangentVBO;
	glGenBuffers(1, &tangentVBO);
  glBindBuffer(GL_ARRAY_BUFFER, tangentVBO);
  glBufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(glm::vec3), &tangents[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(3);
  glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glGenBuffers(1, &bitangentVBO);
  glBindBuffer(GL_ARRAY_BUFFER, bitangentVBO);
  glBufferData(GL_ARRAY_BUFFER, bitangents.size() * sizeof(glm::vec3), &bitangents[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(4);
  glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);


	// Load and Bind Plane Texture
  TexturePlane = loadBMP_custom("texture.bmp");
  TextureID = glGetUniformLocation(programID, "myTextureSampler");
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, TexturePlane);
  glUniform1i(TextureID, 0);
	textureNormal = loadBMP_custom("normal.bmp");
  normalID = glGetUniformLocation(programID, "textureNormal");
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, textureNormal);
  glUniform1i(TextureID, 1);
	textureSpecular = loadBMP_custom("specular.bmp");
  specularID = glGetUniformLocation(programID, "textureSpecular");
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, textureSpecular);
  glUniform1i(TextureID, 2);

	// Clear local vector variables
	vertices.clear(); uvs.clear(); normals.clear();

	// Load Jeep Object file
	res = loadOBJ("jeep.obj", vertices, uvs, normals);
	glGenVertexArrays(1, &jeepVAO);
	glBindVertexArray(jeepVAO);
	// Bind Vertices Data to Buffer
	glGenBuffers(1, &vertexVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind UV Data to Buffer
	glGenBuffers(1, &uvVBO);
	glBindBuffer(GL_ARRAY_BUFFER, uvVBO);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind Normal Data to Buffer
	glGenBuffers(1, &normalVBO);
  glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
  glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec2), &normals[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	drawSize[2] = vertices.size();
	// Load and Bind Jeep Texture
	TextureJeep = loadBMP_custom("jeep_texture.bmp");
	TextureID = glGetUniformLocation(programID, "myTextureSampler");
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, TextureJeep);
  glUniform1i(TextureID, 0);

	// Clear local vector variables
	vertices.clear(); uvs.clear(); normals.clear();

	// Load Airplane Object file
	res = loadOBJ("airplane.obj", vertices, uvs, normals);
	glGenVertexArrays(1, &airplaneVAO);
	glBindVertexArray(airplaneVAO);
	// Bind Vertices Data to Buffer
	glGenBuffers(1, &vertexVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind UV Data to Buffer
	glGenBuffers(1, &uvVBO);
	glBindBuffer(GL_ARRAY_BUFFER, uvVBO);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind Normal Data to Buffer
	glGenBuffers(1, &normalVBO);
  glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
  glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec2), &normals[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	drawSize[3] = vertices.size();
	// Load and Bind Airplane Texture
	TextureAirplane = loadBMP_custom("airplane_texture.bmp");
	TextureID = glGetUniformLocation(programID, "myTextureSampler");
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, TextureAirplane);
  glUniform1i(TextureID, 0);
}

void paintGL(void){
  glClearColor(0.5f, 0.5f, 0.5f, 1.0f); //specify the background color
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Enable depth test
  glClearDepth(1.0f);
  glDepthFunc(GL_LESS);
  glEnable(GL_DEPTH_TEST);
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	if(startAnimation){
		airplaneTheta += 0.01f;
		if(airplaneTheta == 360.0f)
			airplaneTheta = 0.0f;
	}

	planeRender = 1.0f;
  matrixPVMTRS("plane");
  glBindVertexArray(planeVAO);
  glBindTexture(GL_TEXTURE_2D, TexturePlane);
  glDrawArrays(GL_TRIANGLES, 0, drawSize[0]);
	planeRender = 0.0f;

	matrixPVMTRS("jeep");
	glBindVertexArray(jeepVAO);
	glBindTexture(GL_TEXTURE_2D, TextureJeep);
  glDrawArrays(GL_TRIANGLES, 0, drawSize[2]);

	matrixPVMTRS("airplane");
	glBindVertexArray(airplaneVAO);
	glBindTexture(GL_TEXTURE_2D, TextureAirplane);
  glDrawArrays(GL_TRIANGLES, 0, drawSize[3]);

  glutSwapBuffers();
	glFlush();
	glutPostRedisplay();
}

void initializedGL(void){
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
	glEnable(GL_POLYGON_SMOOTH);

  sendDataToOpenGL();
	installShaders();
}

int main(int argc, char *argv[]){
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);
	glutInitDisplayMode( GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH | GLUT_MULTISAMPLE);
  glutInitWindowSize(1024, 1024);
	//glutInitWindowPosition()
	glutCreateWindow(argv[0]); // window title
	glewInit();

	initializedGL();
	glutDisplayFunc(paintGL);

	glutKeyboardFunc(keyboard);
	glutSpecialFunc(move);
	glutPassiveMotionFunc(PassiveMouse);

	glutMainLoop();
	return 0;
}
