	CSCI3260 Assignment 2 Keyboard / Mouse Events

Name: Liu Ho Man
Student ID: 1155077343

Manipulation:
	key '1':					Exit
	key 'j':					Rotate camera to left
	key 'k':					Rotate camera to right

	key 'q':					Make diffuse light brighter
	key 'w':					Make diffuse light darker

	key 'z':					Make specular light brighter
	key 'x':					Make specular light darker

	key '↑':					Move car to its head forward
	key '↓':					Move car to its head backward
	key '←': 					Turn car left
	key '→':					Turn car right

	key 's':					Start/Stop airplane moving

	key ' ':					Start/Stop mouse coordiate controlling
	Mouse UP:					Move the scene down
	Mouse DOWN:				Move the scene up
	Mouse LEFT:				Rotate the scene clockwisely
	Mousr RIGHT:			Rotate the scene anti-clockwisely
