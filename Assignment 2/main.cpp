/*********************************************************
FILE : main.cpp (csci3260 2017-2018 Assignment 2)
*********************************************************/
/*********************************************************
Student Information
Student ID:		1155077343
Student Name:	Liu Ho Man
*********************************************************/


#include <GL/glew.h>
#include <GLUT/glut.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "Library/Dependencies/glm/glm.hpp"
#include "Library/Dependencies/glm/gtc/matrix_transform.hpp"
#include "Library/Dependencies/glm/gtx/rotate_vector.hpp"
#include "Library/Dependencies/glm/gtx/vector_angle.hpp"
#define GL_INIT_CONTEXT() do {  glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH | GLUT_MULTISAMPLE); } while(0)

#include "Library/programChecker.hpp"
#include "Library/gesture.hpp"
#include "Library/objLoader.hpp"
#include "Library/textureLoader.hpp"
#include "Library/compute.hpp"

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
using namespace std;
using glm::vec3;
using glm::mat4;

GLint programID;
GLuint planeVAO, kirbyVAO, jeepVAO, airplaneVAO;
GLuint vertexVBO, uvVBO, normalVBO;
GLsizei drawSize[4];

GLuint TextureID, TexturePlane, TextureKirby, TextureJeep, TextureAirplane;
GLuint textureNormal, textureSpecular, normalID, specularID;

float specularCoefficient = 0.1f, diffuseCoefficient = 50.0f, planeRender = 0.0f;

float carX = 0.0f, carZ = 0.0f, carRotate = 1.0f;
int rotatePress = 0, xOrigin, yOrigin;
float rotation_theta = 45.0f, rotation_alpha = 45.0f;
float airplaneTheta = 0.0f;
bool startMouse = false, mousePosinit = false, startAnimation = true;

void matrixPVMTRS(string object){
  glm::mat4 modelScalingMatrix = glm::mat4(1.0f);
  glm::mat4 modelTransformMatrix = glm::mat4(1.0f);
  glm::mat4 modelRotationMatrix = glm::mat4(1.0f);
	glm::mat4 carModelSelfRotation = glm::rotate(glm::mat4(), glm::radians(carRotate * rotatePress), glm::vec3(0, 1, 0));
	glm::mat4 airplaneSelfRotation = glm::rotate(glm::mat4(), -airplaneTheta, glm::vec3(0, 1, 0)) * glm::rotate(glm::mat4(), -airplaneTheta, glm::vec3(0, 0, 1));
	glm::mat3 VM3x3 = glm::mat3(1.0f);
	// std::cout << "{" << carRotate * rotatePress << "}\n";

	if(object == "plane"){
  	modelScalingMatrix = glm::scale(glm::mat4(), glm::vec3(1.0f, 1.5f, 1.5f));
		modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, 0.0f));;
		modelRotationMatrix = glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(0, 1, 0));
	}
	else if(object == "jeep"){
		modelScalingMatrix = carModelSelfRotation * glm::scale(glm::mat4(), glm::vec3(0.5f, 0.5f, 0.5f));
		modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(2.0f + carX, 0.3f, -1.0f + carZ));;
		modelRotationMatrix = glm::mat4(1.0f);
	}
	else if(object == "airplane"){
		modelScalingMatrix = airplaneSelfRotation * glm::scale(glm::mat4(), glm::vec3(0.15f, 0.15f, 0.15f));
		modelTransformMatrix = glm::translate(glm::mat4(), glm::vec3(5.0f * cos(airplaneTheta), 7.0f, 5.0f * sin(airplaneTheta)));;
		modelRotationMatrix = glm::mat4(1.0f);
	}


  GLint modelScalingMatrixUniformLocation = glGetUniformLocation(programID, "modelScalingMatrix");
  GLint modelTransformMatrixUniformLocation = glGetUniformLocation(programID, "modelTransformMatrix");
  GLint modelRotateMatrixUniformLocation = glGetUniformLocation(programID, "modelRotationMatrix");

  glUniformMatrix4fv(modelScalingMatrixUniformLocation, 1, GL_FALSE, &modelScalingMatrix[0][0]);
  glUniformMatrix4fv(modelTransformMatrixUniformLocation, 1, GL_FALSE, &modelTransformMatrix[0][0]);
	glUniformMatrix4fv(modelRotateMatrixUniformLocation, 1, GL_FALSE, &modelRotationMatrix[0][0]);

  // Projection matrix : 45° Field of View
  glm::mat4 Projection = glm::perspective(glm::radians(45.0f), 1.0f, 2.0f, 40.0f);
  glm::mat4 View = glm::lookAt(
      glm::vec3(20.0f * cos(rotation_theta), 15.0f * sin(rotation_alpha), 20.0f * sin(rotation_theta)), // Camera is at (x,y,z), in World Space
			// glm::vec3(0.0f, 0.0f, 0.0f),
			glm::vec3(0.0f, 2.0f, 0.0f), // and looks at point
      glm::vec3(0.0f, 1.0f, 0.0f)  // Head is up (set to 0,-1,0 to look upside-down)
      );
  glm::mat4 Model = glm::mat4(1.0f);
  glm::mat4 pvm = Projection * View * Model;

	glm::vec3 lightPosition = glm::vec3(0.0f, 8.0f, -2.0f);
	GLuint LightID = glGetUniformLocation(programID, "lightPosition");
	glUniform3f(LightID, lightPosition.x, lightPosition.y, lightPosition.z);

	glm::mat3 VM = glm::mat3(View * Model);
	GLuint VMID = glGetUniformLocation(programID, "VM");
	glUniformMatrix4fv(VMID, 1, GL_FALSE, &VM[0][0]);

  GLuint MatrixID = glGetUniformLocation(programID, "PVM");
	GLuint ViewID = glGetUniformLocation(programID, "viewMatrix");
	GLuint ModelID = glGetUniformLocation(programID, "modelMatrix");
  glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &pvm[0][0]);
	glUniformMatrix4fv(ViewID, 1, GL_FALSE, &View[0][0]);
	glUniformMatrix4fv(ModelID, 1, GL_FALSE, &Model[0][0]);

	GLuint diffuseID = glGetUniformLocation(programID, "diffuseCoefficient");
	glUniform1f(diffuseID, diffuseCoefficient);
	GLuint specularID = glGetUniformLocation(programID, "specularCoefficient");
	glUniform1f(specularID, specularCoefficient);

	GLuint renderID = glGetUniformLocation(programID, "planeRender");
	glUniform1f(renderID, planeRender);
}

void sendDataToOpenGL(){

  std::vector <glm::vec3> vertices;
  std::vector <glm::vec2> uvs;
  std::vector <glm::vec3> normals;
	std::vector <glm::vec3> tangents;
	std::vector <glm::vec3> bitangents;

	// Load Plane Object file
  bool res = loadOBJ("plane.obj", vertices, uvs, normals);
	computeTangent(vertices, uvs, normals, tangents, bitangents);
  glGenVertexArrays(1, &planeVAO);
  glBindVertexArray(planeVAO);
  // Bind Vertices Data to Buffer
  glGenBuffers(1, &vertexVBO);
  glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind UV Data to Buffer
  glGenBuffers(1, &uvVBO);
  glBindBuffer(GL_ARRAY_BUFFER, uvVBO);
  glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind Normal Data to Buffer
	glGenBuffers(1, &normalVBO);
  glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
  glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec2), &normals[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  drawSize[0] = vertices.size();
	GLuint tangentVBO, bitangentVBO;
	glGenBuffers(1, &tangentVBO);
  glBindBuffer(GL_ARRAY_BUFFER, tangentVBO);
  glBufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(glm::vec3), &tangents[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(3);
  glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glGenBuffers(1, &bitangentVBO);
  glBindBuffer(GL_ARRAY_BUFFER, bitangentVBO);
  glBufferData(GL_ARRAY_BUFFER, bitangents.size() * sizeof(glm::vec3), &bitangents[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(4);
  glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);


	// Load and Bind Plane Texture
  TexturePlane = loadBMP_custom("texture.bmp");
  TextureID = glGetUniformLocation(programID, "myTextureSampler");
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, TexturePlane);
  glUniform1i(TextureID, 0);
	textureNormal = loadBMP_custom("normal.bmp");
  normalID = glGetUniformLocation(programID, "textureNormal");
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, textureNormal);
  glUniform1i(TextureID, 1);
	textureSpecular = loadBMP_custom("specular.bmp");
  specularID = glGetUniformLocation(programID, "textureSpecular");
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, textureSpecular);
  glUniform1i(TextureID, 2);

	// Clear local vector variables
	vertices.clear(); uvs.clear(); normals.clear();

	// Load Jeep Object file
	res = loadOBJ("jeep.obj", vertices, uvs, normals);
	glGenVertexArrays(1, &jeepVAO);
	glBindVertexArray(jeepVAO);
	// Bind Vertices Data to Buffer
	glGenBuffers(1, &vertexVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind UV Data to Buffer
	glGenBuffers(1, &uvVBO);
	glBindBuffer(GL_ARRAY_BUFFER, uvVBO);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind Normal Data to Buffer
	glGenBuffers(1, &normalVBO);
  glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
  glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec2), &normals[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	drawSize[2] = vertices.size();
	// Load and Bind Jeep Texture
	TextureJeep = loadBMP_custom("jeep_texture.bmp");
	TextureID = glGetUniformLocation(programID, "myTextureSampler");
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, TextureJeep);
  glUniform1i(TextureID, 0);

	// Clear local vector variables
	vertices.clear(); uvs.clear(); normals.clear();

	// Load Airplane Object file
	res = loadOBJ("airplane.obj", vertices, uvs, normals);
	glGenVertexArrays(1, &airplaneVAO);
	glBindVertexArray(airplaneVAO);
	// Bind Vertices Data to Buffer
	glGenBuffers(1, &vertexVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind UV Data to Buffer
	glGenBuffers(1, &uvVBO);
	glBindBuffer(GL_ARRAY_BUFFER, uvVBO);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	// Bind Normal Data to Buffer
	glGenBuffers(1, &normalVBO);
  glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
  glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec2), &normals[0], GL_STATIC_DRAW);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	drawSize[3] = vertices.size();
	// Load and Bind Airplane Texture
	TextureAirplane = loadBMP_custom("airplane_texture.bmp");
	TextureID = glGetUniformLocation(programID, "myTextureSampler");
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, TextureAirplane);
  glUniform1i(TextureID, 0);

	// for(int i=0; i<vertices.size(); ++i){
	// 	std::cout << "{" << vertices[i].x << " " << vertices[i].y << " " << vertices[i].z << "}" << "\n";
	// 	std::cout << "{" << uvs[i].x << " " << uvs[i].y << "}" << "\n\n";
	// }

}

void paintGL(void){
  glClearColor(0.5f, 0.5f, 0.5f, 1.0f); //specify the background color
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Enable depth test
  glClearDepth(1.0f);
  glDepthFunc(GL_LESS);
  glEnable(GL_DEPTH_TEST);
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	if(startAnimation){
		airplaneTheta += 0.1f;
		if(airplaneTheta == 360.0f)
			airplaneTheta = 0.0f;
	}

	planeRender = 1.0f;
  matrixPVMTRS("plane");
  glBindVertexArray(planeVAO);
  glBindTexture(GL_TEXTURE_2D, TexturePlane);
  glDrawArrays(GL_TRIANGLES, 0, drawSize[0]);
	planeRender = 0.0f;

	matrixPVMTRS("jeep");
	glBindVertexArray(jeepVAO);
	glBindTexture(GL_TEXTURE_2D, TextureJeep);
  glDrawArrays(GL_TRIANGLES, 0, drawSize[2]);

	matrixPVMTRS("airplane");
	glBindVertexArray(airplaneVAO);
	glBindTexture(GL_TEXTURE_2D, TextureAirplane);
  glDrawArrays(GL_TRIANGLES, 0, drawSize[3]);

  glutSwapBuffers();
	glFlush();
	glutPostRedisplay();
}

void initializedGL(void){
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
	glEnable(GL_POLYGON_SMOOTH);

  sendDataToOpenGL();
	programID = installShaders();
}

int main(int argc, char *argv[]){
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);
	glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH | GLUT_MULTISAMPLE);
  glutInitWindowSize(1024, 1024);
	//glutInitWindowPosition()
	glutCreateWindow(argv[0]); // window title
	glewInit();

	initializedGL();
	glutDisplayFunc(paintGL);

	glutKeyboardFunc(keyboardClick);
	glutSpecialFunc(arrowKey);
	glutPassiveMotionFunc(mouseCoordinate);

	glutMainLoop();
	return 0;
}
